from tkinter import *
from tkinter import ttk
from PIL import Image, ImageTk
import sqlite3

# Create a database connection
conn = sqlite3.connect("hotel_rooms.db")

# Create a cursor object to execute SQL commands
cursor = conn.cursor()

# Create the rooms table if it doesn't exist
cursor.execute("""
CREATE TABLE IF NOT EXISTS rooms (
    room_number INTEGER PRIMARY KEY,
    type TEXT,
    status TEXT,
    cost_per_day INTEGER,
    capacity INTEGER
)
""")

# Insert sample data into the rooms table
sample_data = [
    (101, 'Single beded', 'Available', 1500, 1),
    (102, 'Double beded', 'Occupied', 2500, 2),
    (103, 'Single beded', 'Available', 1500, 1),
    (104, 'Double beded', 'Available', 2500, 2),
    (105, 'Triple beded', 'Occupied', 4000, 3),
    (201, 'Single beded', 'Available', 1500, 1),
    (202, 'Double beded', 'Available', 2500, 2),
    (203, 'Single beded', 'Occupied', 1500, 1),
    (204, 'Double beded', 'Available', 2500, 2),
    (205, 'Triple beded', 'Available', 4000, 3),
    (301, 'Single beded', 'Occupied', 1500, 1),
    (302, 'Double beded', 'Available', 2500, 2),
    (303, 'Single beded', 'Available', 1500, 1),
    (304, 'Double beded', 'Occupied', 2500, 2),
    (305, 'Triple beded', 'Available', 4000, 3)
]

cursor.executemany("INSERT INTO rooms (room_number, type, status, cost_per_day, capacity) VALUES (?, ?, ?, ?, ?)", sample_data)

def destroy_window(window):
    window.destroy()

def set_background(window, image_path):
    bg_img = Image.open(image_path)
    bg_img = bg_img.resize((1550, 800))
    bg_img = ImageTk.PhotoImage(bg_img)
    bg_lbl = Label(window, image=bg_img)
    bg_lbl.image = bg_img
    bg_lbl.place(x=0, y=0)

def main_page():
    global main_window  # Declare main_window as global
    
    def logout():
        main_window.destroy()

    def customer_details_window():
        def save_customer_details():
            customer_id = id_entry.get()
            customer_name = name_entry.get()
            customer_dob = dob_entry.get()
            customer_address = address_entry.get()
            customer_phone = phone_entry.get()
            
            print("Customer ID:", customer_id)
            print("Customer Name:", customer_name)
            print("Customer Date of Birth:", customer_dob)
            print("Customer Address:", customer_address)
            print("Customer Phone:", customer_phone)
            save_message.config(text="Saved Successfully!")

        customer_window = Toplevel(main_window)
        customer_window.title("Customer Details")
        customer_window.geometry("1550x800+0+0")
        customer_window.attributes('-topmost', True)
        set_background(customer_window, 'inside.jpg')
        
        # Heading
        customer_heading_label = Label(customer_window, text="Customer Details", font=("Times New Roman", 24, "bold"))
        customer_heading_label.pack(pady=20)

        id_label = Label(customer_window, text="ID:", font=("Times New Roman", 14))
        id_label.pack()
        id_entry = Entry(customer_window)
        id_entry.pack()

        name_label = Label(customer_window, text="Name:", font=("Times New Roman", 14))
        name_label.pack()
        name_entry = Entry(customer_window)
        name_entry.pack()

        dob_label = Label(customer_window, text="Date of Birth:", font=("Times New Roman", 14))
        dob_label.pack()
        dob_entry = Entry(customer_window)
        dob_entry.pack()

        address_label = Label(customer_window, text="Address:", font=("Times New Roman", 14))
        address_label.pack()
        address_entry = Entry(customer_window)
        address_entry.pack()

        phone_label = Label(customer_window, text="Phone:", font=("Times New Roman", 14))
        phone_label.pack()
        phone_entry = Entry(customer_window)
        phone_entry.pack()

        save_button = Button(customer_window, text="Save", command=save_customer_details, width=10, height=2, font=("Times New Roman", 14))
        save_button.pack(pady=10)

        save_message = Label(customer_window, text="", font=("Times New Roman", 12, "bold"))
        save_message.pack()

        # Back Button
        back_button = Button(customer_window, text="Back", command=lambda: destroy_window(customer_window), width=10, height=2, font=("Times New Roman", 14))
        back_button.place(relx=1.0, rely=0.0, anchor=NE)

    def hotel_details_page():
        hotel_details_window = Toplevel(main_window)
        hotel_details_window.title("Hotel Details")
        hotel_details_window.geometry("1550x800+0+0")
        hotel_details_window.attributes('-topmost', True)
        set_background(hotel_details_window, 'inside.jpg')
        
        # Heading
        hotel_details_label = Label(hotel_details_window, text="Hotel Details", font=("Times New Roman", 24, "bold"))
        hotel_details_label.pack(pady=20)

        hotel_info = "Hotel ID: 1000\nName: Annex Hotel\nAddress: Rd no 7, Andheri East, Mumbai, Maharashtra,pincode:400069\nPhone: 08823 69876\nEmail: annex2018@gmail.com"
        hotel_info_label = Label(hotel_details_window, text=hotel_info, font=("Times New Roman", 16))
        hotel_info_label.pack()

        # Back Button
        back_button = Button(hotel_details_window, text="Back", command=lambda: destroy_window(hotel_details_window), width=10, height=2, font=("Arial", 14))
        back_button.place(relx=1.0, rely=0.0, anchor=NE)

    def room_details_window():
        room_window = Toplevel(main_window)
        room_window.title("Room Details")
        room_window.geometry("1550x800+0+0")

        # Heading
        room_heading_label = Label(room_window, text="Room Details", font=("Times New Roman", 24, "bold"))
        room_heading_label.pack(pady=20)

        # Retrieve room data from the database
        cursor.execute("SELECT * FROM rooms")
        rooms_data = cursor.fetchall()

        # Create a treeview to display room data
        room_tree = ttk.Treeview(room_window, columns=("Room Number", "Type", "Status", "Cost per Day", "Capacity"), height=15)
        room_tree.heading("Room Number", text="Room Number")
        room_tree.heading("Type", text="Type")
        room_tree.heading("Status", text="Status")
        room_tree.heading("Cost per Day", text="Cost per Day")
        room_tree.heading("Capacity", text="Capacity")
        room_tree.pack()

        # Insert room data into the treeview
        for row in rooms_data:
            room_tree.insert("", "end", values=row)

        # Back button
        back_button = Button(room_window, text="Back", command=room_window.destroy, width=10, height=2, font=("Times New Roman", 14))
        back_button.pack(pady=20)

    main_window = Toplevel(root)
    main_window.title("Main Page")
    main_window.geometry("1550x800+0+0")

    # Heading
    heading_label = Label(main_window, text="Hotel Management System", font=("Times New Roman", 20, "bold"))
    heading_label.pack(pady=20)

    # Menu Button
    menu_button = Button(main_window, text="Menu", width=25, height=4)
    menu_button.pack(pady=20)

    # Space after menu button
    space_label = Label(main_window, text="")
    space_label.pack()

    # Customer Button
    customer_button = Button(main_window, text="Customer", font=("Times New Roman", 14), width=20, height=2, command=customer_details_window)
    customer_button.pack()

    # Space after Customer button
    space_label = Label(main_window, text="")
    space_label.pack()

    # Hotel Button
    hotel_button = Button(main_window, text="Hotel", font=("Times New Roman", 14), width=20, height=2, command=hotel_details_page)
    hotel_button.pack()

    # Space after Hotel button
    space_label = Label(main_window, text="")
    space_label.pack()

    # Room Button
    room_button = Button(main_window, text="Room", font=("Times New Roman", 14), width=20, height=2, command=room_details_window)
    room_button.pack()

    # Logout Button
    logout_button = Button(main_window, text="Logout", command=logout, font=("Times New Roman", 14), width=10, height=2)
    logout_button.place(relx=1.0, rely=0.0, anchor=NE)

def signup_window():
    def back():
        signup_window.destroy()

    def signup():
        email = email_entry.get()
        username = username_entry.get()
        password = password_entry.get()
        print("Email:", email)
        print("Username:", username)
        print("Password:", password)
        
        signup_window.destroy()
        main_page()

    signup_window = Toplevel(root)
    signup_window.title("Sign Up")
    signup_window.geometry("1550x800+0+0")

    # Heading
    heading_label = Label(signup_window, text="Hotel Management System", font=("Times New Roman", 20, "bold"))
    heading_label.pack(pady=20)

    email_label = Label(signup_window, text="Email:")
    email_label.place(relx=0.5, rely=0.35, anchor=CENTER)
    email_entry = Entry(signup_window)
    email_entry.place(relx=0.5, rely=0.4, anchor=CENTER)

    username_label = Label(signup_window, text="Username:")
    username_label.place(relx=0.5, rely=0.45, anchor=CENTER)
    username_entry = Entry(signup_window)
    username_entry.place(relx=0.5, rely=0.5, anchor=CENTER)

    password_label = Label(signup_window, text="Password:")
    password_label.place(relx=0.5, rely=0.55, anchor=CENTER)
    password_entry = Entry(signup_window, show="*") 
    password_entry.place(relx=0.5, rely=0.6, anchor=CENTER)

    signup_button = Button(signup_window, text="Create Account", command=signup, width=20, height=2)
    signup_button.place(relx=0.5, rely=0.7, anchor=CENTER)

    back_button = Button(signup_window, text="Back", command=back, width=20, height=2)
    back_button.place(relx=1.0, rely=0.0, anchor=NE)

def login_window():
    def back():
        login_window.destroy()

    def login():
        username = username_entry.get()
        password = password_entry.get()
        print("Username:", username)
        print("Password:", password)
        
        login_window.destroy()
        main_page()

    login_window = Toplevel(root)
    login_window.title("Login")
    login_window.geometry("1550x800+0+0")

    username_label = Label(login_window, text="Username:")
    username_label.place(relx=0.5, rely=0.4, anchor=CENTER)
    username_entry = Entry(login_window)
    username_entry.place(relx=0.5, rely=0.45, anchor=CENTER)

    password_label = Label(login_window, text="Password:")
    password_label.place(relx=0.5, rely=0.55, anchor=CENTER)
    password_entry = Entry(login_window, show="*")
    password_entry.place(relx=0.5, rely=0.6, anchor=CENTER)

    login_button = Button(login_window, text="Login", command=login, width=20, height=2)
    login_button.place(relx=0.5, rely=0.7, anchor=CENTER)

    back_button = Button(login_window, text="Back", command=back, width=20, height=2)
    back_button.place(relx=1.0, rely=0.0, anchor=NE)

class Home():
    def __init__(self,root):
        self.bg_img=Image.open('inside.jpg')
        self.bg_img=self.bg_img.resize((1550,800))
        self.bg_img=ImageTk.PhotoImage(self.bg_img)
        self.bg_lbl=Label(root, image = self.bg_img)
        self.bg_lbl.place(x=0,y=0)

root = Tk()
root.title("Hotel Management System")
root.geometry("1550x800+0+0")
home = Home(root)

def login():
    login_window()

def signup():
    signup_window()

# Heading
heading_label = Label(root, text="Hotel Management System", font=("Times New Roman", 20, "bold"))
heading_label.pack(pady=20)

# Login button
login_button = Button(root, text="Login", command=login, font=("Times New Roman", 14), width=20, height=2)
login_button.pack(side=LEFT, expand=True, padx=10, pady=10)

# Sign up button
signup_button = Button(root, text="Sign Up", command=signup, font=("Times New Roman", 14), width=20, height=2)
signup_button.pack(side=LEFT, expand=True, padx=10, pady=10)

root.mainloop()
